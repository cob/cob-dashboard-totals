var Dashboard = require('cob-dashboard-core');


TotalsDashboard = function() { Dashboard.apply(this, arguments);};
TotalsDashboard.prototype = Object.create(Dashboard.prototype);

TotalsDashboard.prototype.parseQueryResult = function (itemIndex,data) {
    return data.hits.total;
};

TotalsDashboard.prototype.buildItemRequest = function (item) {
    return {
        url : (item.queryURL||this.defaultQueryURL).replace("{{query}}",encodeURIComponent(item.query)),
        data : "",
        type: "GET"
    };
};


TotalsDashboard.prototype.buildHtml = function () {
    $('#'+this.dashboardId).addClass("db-totals");
    $('#'+this.dashboardId).append("<table>");
    this.$table = $('#'+this.dashboardId + " table");
    for( var row=0; row < this.items.length; row++) {
        var item = this.items[row];
        var $newTr = $('<tr><td class="db-button"/><td class="db-item-name"/><td class="db-item-subtext"/><td class="db-result""><a><span id="'+row+'"/></a></td></tr>');
        $('td.db-item-name', $newTr).html(item.text);
        if(item.subtext) {
            $('td.db-item-subtext', $newTr).html(item.subtext);
        }
        if(item.addLink) {
            $('td.db-button', $newTr).html('<a href="'+item.addLink+'"><i class="icon-plus-sign"></i></a>');
        }
        $('.db-result a',$newTr).prop('href',  (item.queryHref||this.defaultQueryHref).replace("{{query}}",encodeURIComponent(item.query)).replace("{{view}}",item.view||this.defaulView))
        $('#'+row,$newTr).attr('class', item.resultClass||"label label-info");
        this.$table.append($newTr);
    }
};

TotalsDashboard.prototype.updateItem = function(row) {
    //Sinalizar se o valor mudou:
    var newValue = cob.utils.numbers.format(this.values[row],0);
    if( $('#'+row,this.$table).text() != "" && $('#'+row,this.$table).text() != newValue) {
        this.$table.parent().parent().addClass("flash").removeClass("flash",2000);
        $('#'+row,this.$table).fadeIn(1000).fadeOut(1000).fadeIn(1000).fadeOut(1000).fadeIn(1000).fadeOut(1000).fadeIn(1000);
    }
    $('#'+row,this.$table).text(newValue);
    if(newValue == "0") {
        $('#'+row,this.$table).attr("class","default");
    } else {
        $('#'+row,this.$table).attr('class', this.items[row].resultClass||"default");
    }
};



module.exports = TotalsDashboard;